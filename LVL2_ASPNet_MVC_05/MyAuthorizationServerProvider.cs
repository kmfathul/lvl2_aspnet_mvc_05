﻿using LVL2_ASPNet_MVC_05.Models;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace LVL2_ASPNet_MVC_05
{
    public class MyAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        TESTAPIEntities db = new TESTAPIEntities();
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var u = db.tbl_ms_user.Where(x => x.pk_ms_user_id == 1).FirstOrDefault();
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            if (context.UserName == u.username && context.Password == u.password)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, "admin"));
                identity.AddClaim(new Claim("username", "admin"));
                identity.AddClaim(new Claim(ClaimTypes.Name, "Petrus Yudi"));
                context.Validated(identity);
            }
            else if (context.UserName == "user" && context.Password == "user")
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, "user"));
                identity.AddClaim(new Claim("username", "user"));
                identity.AddClaim(new Claim(ClaimTypes.Name, "Yudi Irwanto"));
                context.Validated(identity);
            }
            else
            {
                context.SetError("Invalid Grant", "Provided username and password is incorrect");
                return;
            }
        }
    }
}